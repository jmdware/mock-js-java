/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DependencyMapperTest {

    private static final String X_NAME = "x";

    private static final String Y_NAME = "why";

    private static final String SERVICE_NAME = "serviceDependency";

    private final Input input = new Input();

    private final DependencyMapper mapper = new DependencyMapper();

    private final int x = 100;

    private final String y = "string";

    @Mock
    private Service service;

    @Test
    public void testInspect() {
        Map<String, ResolvableDependency> nameToFactory = mapper.map(input);

        input.x = x;
        input.y = y;
        input.notADependency = mock(Service.class);
        input.serviceDependency = service;

        assertThat(nameToFactory.values().stream()
                .filter(v -> !(v.getProvider() instanceof DependencyField))
                .findAny()
                .isPresent(), equalTo(false));

        assertThat(nameToFactory.get(X_NAME).getProvider().get(), equalTo(x));
        assertThat(nameToFactory.get(Y_NAME).getProvider().get(), equalTo(y));
        assertThat(nameToFactory.get(SERVICE_NAME).getProvider().get(), sameInstance(service));

        assertThat(nameToFactory.size(), equalTo(3));
    }

    public static class Input {
        @Dependency
        private int x;

        @Dependency(Y_NAME)
        private String y;

        private Service notADependency;

        @Dependency
        private Service serviceDependency;
    }

    public interface Service { }
}
