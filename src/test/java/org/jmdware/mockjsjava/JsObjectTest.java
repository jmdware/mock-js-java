/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.junit.Before;
import org.junit.Test;

public class JsObjectTest {

    private final ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");

    private ScriptObjectMirror actualJsObject;

    private JsObject jsObject;

    @Before
    public void before() throws ScriptException {
        engine.eval("var o = { x: 1, y: 2.3, z: function(s) { return s + '-' + s; } };");
        actualJsObject = (ScriptObjectMirror) engine.get("o");
        jsObject = new JsObject(engine, actualJsObject);
    }

    @Test
    public void testGet() {
        assertThat(jsObject.get("x"), equalTo(1));
        assertThat(jsObject.get("y"), equalTo(2.3));
        assertThat(jsObject.get("not-there"), nullValue());
    }

    @Test
    public void testHas() {
        assertThat(jsObject.has("x"), equalTo(true));
        assertThat(jsObject.has("y"), equalTo(true));
        assertThat(jsObject.has("z"), equalTo(true));
        assertThat(jsObject.has("not-there"), equalTo(false));
    }

    @Test
    public void testPut() {
        jsObject.put("x", 100);

        assertThat(jsObject.has("x"), equalTo(true));
        assertThat(jsObject.get("x"), equalTo(100));
        assertThat(actualJsObject.get("x"), equalTo(100));

        jsObject.put("newProperty", "just added");

        assertThat(jsObject.has("newProperty"), equalTo(true));
        assertThat(jsObject.get("newProperty"), equalTo("just added"));
        assertThat(actualJsObject.get("newProperty"), equalTo("just added"));
    }

    @Test
    public void testRemove() {
        assertThat(jsObject.remove("x"), equalTo(1));

        assertThat(jsObject.has("x"), equalTo(false));
        assertThat(actualJsObject.containsKey("x"), equalTo(false));
    }

    @Test
    public void testInvokeMethod() {
        assertThat(jsObject.invokeMethod("z", "foo"), equalTo("foo-foo"));
    }

    @Test
    public void testProxy() {
        Proxy proxy = jsObject.as(Proxy.class);

        assertThat(proxy, not(nullValue()));
        assertThat(proxy.z("foo"), equalTo("foo-foo"));

        try {
            jsObject.as(SignatureDoesNotMatch.class);
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), containsString(SignatureDoesNotMatch.class.toString()));
        }
    }

    public interface Proxy {
        String z(String s);
    }

    public interface SignatureDoesNotMatch {
        void noMatch();
    }
}
