package org.jmdware.mockjsjava.example;

/**
 * Proxy interface for a JavaScript Adder.
 */
public interface Adder {
    int add(int n1, int n2);
}
