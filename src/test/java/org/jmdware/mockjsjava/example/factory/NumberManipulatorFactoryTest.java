package org.jmdware.mockjsjava.example.factory;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.jmdware.mockjsjava.Dependency;
import org.jmdware.mockjsjava.InjectJsFactory;
import org.jmdware.mockjsjava.example.Adder;
import org.jmdware.mockjsjava.example.Divider;
import org.jmdware.mockjsjava.example.NumberManipulator;
import org.jmdware.mockjsjava.junit.MockJsJava;
import org.jmdware.mockjsjava.junit.MockJsJavaJUnitRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

/**
 * Tests a factory function called {@code NumberManipulator}. Factory functions
 * directly return objects and must not be used with the {@code new} keyword.
 *
 * <pre>
 * var manipulator = NumberManipulator(...);
 * </pre>
 */
@RunWith(MockJsJavaJUnitRunner.class)
@MockJsJava("src/test/java/org/jmdware/mockjsjava/example/factory/NumberManipulator.js")
public class NumberManipulatorFactoryTest {
    @Dependency
    private final int num = 4;

    @Mock
    @Dependency("adder")
    private Adder adderMock;

    @Mock
    @Dependency("divider")
    private Divider dividerMock;

    @InjectJsFactory(value = "NumberManipulator")
    private NumberManipulator numberManipulator;

    @Test
    public void testAdd() {
        when(adderMock.add(4, 5)).thenReturn(20);

        assertThat(numberManipulator.add(5), equalTo(20));

        verify(adderMock).add(4, 5);
    }

    @Test
    public void testDivide() {
        when(dividerMock.divide(4, 2)).thenReturn(2D);

        assertThat(numberManipulator.divide(2), equalTo(2D));

        verify(dividerMock).divide(4, 2);
    }
}
