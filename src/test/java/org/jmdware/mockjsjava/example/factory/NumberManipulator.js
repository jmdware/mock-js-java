function NumberManipulator(num, adder, divider) {
    return {
        add: function(n) {
            return adder.add(num, n);
        },

        divide: function(n) {
            return divider.divide(num, n);
        }
    };
}

NumberManipulator.$inject = [ "num", "adder", "divider" ];
