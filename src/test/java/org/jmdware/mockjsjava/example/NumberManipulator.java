package org.jmdware.mockjsjava.example;

/**
 * Proxy interface for a JavaScript number manipulator.
 */
public interface NumberManipulator {
    int add(int i);

    double divide(double d);
}
