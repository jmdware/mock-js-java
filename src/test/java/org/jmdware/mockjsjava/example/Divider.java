package org.jmdware.mockjsjava.example;

/**
 * Proxy interface for a JavaScript divider.
 */
public interface Divider {
    double divide(double n1, double n2);
}
