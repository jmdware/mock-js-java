function NumberManipulator(num, adder, divider) {
    this.num = num;
    this.adder = adder;
    this.divider = divider;
}

NumberManipulator.prototype = {
    add: function(n) {
        return this.adder.add(this.num, n);
    },

    divide: function(n) {
        return this.divider.divide(this.num, n);
    }
};
