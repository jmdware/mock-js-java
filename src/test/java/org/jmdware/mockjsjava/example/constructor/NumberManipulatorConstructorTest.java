package org.jmdware.mockjsjava.example.constructor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.jmdware.mockjsjava.Dependency;
import org.jmdware.mockjsjava.InjectJsCtor;
import org.jmdware.mockjsjava.example.Adder;
import org.jmdware.mockjsjava.example.Divider;
import org.jmdware.mockjsjava.example.NumberManipulator;
import org.jmdware.mockjsjava.junit.MockJsJava;
import org.jmdware.mockjsjava.junit.MockJsJavaJUnitRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

/**
 * Tests a constructor function called {@code NumberManipulator}. Constructors
 * are invoked using the {@code new} keyword in JavaScript.
 *
 * <pre>
 * var manipulator = new NumberManipulator(...);
 * </pre>
 */
@RunWith(MockJsJavaJUnitRunner.class)
@MockJsJava("src/test/java/org/jmdware/mockjsjava/example/constructor/NumberManipulator.js")
public class NumberManipulatorConstructorTest {
    @Dependency("num")
    private final int number = 4;

    @Mock
    @Dependency
    private Adder adder;

    @Mock
    @Dependency
    private Divider divider;

    @InjectJsCtor(value = "NumberManipulator", dependencies = { "num", "adder", "divider" })
    private NumberManipulator numberManipulator;

    @Test
    public void testAdd() {
        when(adder.add(4, 5)).thenReturn(20);

        assertThat(numberManipulator.add(5), equalTo(20));

        verify(adder).add(4, 5);
    }

    @Test
    public void testDivide() {
        when(divider.divide(4, 2)).thenReturn(2D);

        assertThat(numberManipulator.divide(2), equalTo(2D));

        verify(divider).divide(4, 2);
    }
}
