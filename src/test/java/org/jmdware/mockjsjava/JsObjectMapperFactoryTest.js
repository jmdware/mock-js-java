/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var MockJsJava = (function() {
    var MockJsJava = {};

    MockJsJava.Controller = Controller;

    function Controller(number, adder, repeater) {
        var controller = {};

        controller.add = add;
        controller.repeat = repeat;

        function add(addend) {
            return adder.add(number, addend);
        }

        function repeat(string) {
            return repeater.repeat(string, number);
        }

        return controller;
    }

    Controller.$inject = [ 'num', 'adder', 'repeater' ];

    return MockJsJava;
})();
