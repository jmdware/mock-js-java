/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.io.InputStreamReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class JsObjectMapperConstructorTest {

    private final ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");

    @Dependency("num")
    private final int number = 4;

    @Dependency
    @Mock
    private Adder adder;

    @Dependency
    @Mock
    private StringRepeater repeater;

    /**
     * Should be injected using {@code $inject} property.
     */
    @InjectJsCtor(value = "JsObjectMapperConstructorTest.Controller", dependencies = { "num", "adder", "repeater" })
    private Controller controller;

    /**
     * Should be injected using explicitly defined dependencies.
     */
    @InjectJsCtor(value = "JsObjectMapperConstructorTest.Controller", dependencies = { "num", "adder", "repeater" })
    private JsObject jsObject;

    /**
     * The result of the operation, whether by {@link #controller} or
     * {@link #jsObject}.
     */
    private Object result;

    @Before
    public void loadScript() throws Exception {
        InputStream js = getClass().getResourceAsStream(JsObjectMapperConstructorTest.class.getSimpleName() + ".js");

        try (InputStreamReader reader = new InputStreamReader(js)) {
            engine.eval(reader);
        }
    }

    @Before
    public void setupAdder() {
        when(adder.add(anyInt(), anyInt())).thenAnswer(invocation -> {
            Object[] arguments = invocation.getArguments();

            int d1 = ((Number) arguments[0]).intValue();
            int d2 = ((Number) arguments[1]).intValue();

            return d1 + d2;
        });
    }

    @Before
    public void setupStringRepeater() {
        when(repeater.repeat(any(String.class), anyInt())).thenAnswer(invocation -> {
            Object[] arguments = invocation.getArguments();

            String string = (String) arguments[0];
            int times = (Integer) arguments[1];

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < times; i++) {
                sb.append(string);
            }

            return sb.toString();
        });
    }

    @Test
    public void testJsObject() {
        whenTestClassIsMapped();

        thenJsObjectIsNotNull();

        whenControllerAdd(10);
        thenResultIs(number + 10);

        whenJsObjectAdd(20);
        thenResultIs(number + 20);

        whenControllerRepeat("foo");
        thenResultIs("foofoofoofoo");

        whenJsObjectRepeat("foo");
        thenResultIs("foofoofoofoo");
    }

    private void whenControllerAdd(int addend) {
        result = controller.add(addend);
    }

    private void whenJsObjectAdd(int addend) {
        result = jsObject.invokeMethod("add", addend);
    }

    private void whenControllerRepeat(String s) {
        result = controller.repeat(s);
    }

    private void whenJsObjectRepeat(String s) {
        result = jsObject.invokeMethod("repeat", s);
    }

    private void thenResultIs(Object o) {
        assertThat(result, equalTo(o));
    }

    private void whenTestClassIsMapped() {
        new JsObjectMapper(engine).map(this);
    }

    private void thenJsObjectIsNotNull() {
        assertThat(jsObject, not(nullValue()));
    }

    public interface Controller {
        int add(int n);

        String repeat(String s);
    }

    public interface Adder {
        int add(int n1, int n2);
    }

    public interface StringRepeater {
        String repeat(String string, int times);
    }
}
