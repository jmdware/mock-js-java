/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

public class JsObjectMapper {

    private final ScriptEngine engine;

    public JsObjectMapper(ScriptEngine engine) {
        this.engine = Objects.requireNonNull(engine, "engine");
    }

    public void map(Object test) {
        DependencyMapper fieldMapper = new DependencyMapper();

        // First pass, find the @Dependency fields . . .
        Map<String, ResolvableDependency> nameToDep = fieldMapper.map(test);

        // . . . then use them to build JavaScript objects.
        InjectMapper injectMapper = new InjectMapper(nameToDep);

        Map<String, ResolvableDependency> map = injectMapper.map(test);

        map.entrySet().forEach(e -> {
            ResolvableDependency dep = e.getValue();

            try {
                dep.getField().set(test, dep.getProvider().get());
            } catch (IllegalAccessException exc) {
                throw new IllegalArgumentException(dep.getField().toString(), exc);
            }
        });
    }

    /**
     * Finds fields annotated {@link InjectJsCtor} or {@link InjectJsFactory}
     * and builds providers for them using the other dependencies.
     */
    private class InjectMapper extends FieldMapper {

        private final Map<String, ResolvableDependency> nameToDep;

        private InjectMapper(Map<String, ResolvableDependency> nameToDep) {
            this.nameToDep = nameToDep;
        }

        @Override
        protected Optional<NamedProvider> accept(Object object, Field field) {
            InjectJsCtor ctorAnnotation = field.getAnnotation(InjectJsCtor.class);
            InjectJsFactory factoryAnnotation = field.getAnnotation(InjectJsFactory.class);

            if (ctorAnnotation == null && factoryAnnotation == null) {
                return Optional.empty();
            }

            if (ctorAnnotation != null && factoryAnnotation != null) {
                throw new IllegalArgumentException(field.getName() + " has both "
                        + InjectJsCtor.class + " and " + InjectJsFactory.class
                        + " but may only have one");
            }

            NamedProvider provider;

            if (ctorAnnotation != null) {
                provider = wrapCtor(field, ctorAnnotation);
            } else {
                provider = wrapFactory(field, factoryAnnotation);
            }

            return Optional.of(provider);
        }

        private NamedProvider wrapCtor(Field field, InjectJsCtor annotation) {
            String ctorName = annotation.value();

            Supplier<List<String>> dependencies;

            if (annotation.dependencies().length > 0) {
                dependencies = () -> Arrays.asList(annotation.dependencies());
            } else {
                dependencies = () -> get$inject(ctorName);
            }

            return createNamedProvider(field, "new " + ctorName, dependencies);
        }

        private NamedProvider wrapFactory(Field field, InjectJsFactory annotation) {
            String factoryName = annotation.value();

            Supplier<List<String>> dependencies;

            if (annotation.dependencies().length > 0) {
                dependencies = () -> Arrays.asList(annotation.dependencies());
            } else {
                dependencies = () -> get$inject(factoryName);
            }

            return createNamedProvider(field, factoryName, dependencies);
        }

        private NamedProvider createNamedProvider(
                Field field,
                String function,
                Supplier<List<String>> dependencies) {
            ObjectProvider provider;

            Class<?> type = field.getType();

            if (type == JsObject.class) {
                provider = jsObjectProvider(createObjectProvider(function, dependencies));
            } else if (type.isInterface()) {
                provider = proxyProvider(createObjectProvider(function, dependencies), type);
            } else {
                throw new IllegalArgumentException(InjectJsCtor.class
                        + " may only be applied to interfaces or " + JsObject.class
                        + " but was applied to " + field);
            }

            return new NamedProvider(field.getName(), provider);
        }

        private ObjectProvider createObjectProvider(String call, Supplier<List<String>> dependencies) {
            return () -> {
                List<String> names = dependencies.get();

                List<Object> deps = resolveDependencies(names);

                Object object;

                Bindings bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);

                for (int i = 0; i < names.size(); i++) {
                    String name = names.get(i);

                    if (bindings.containsKey(name)) {
                        throw new IllegalArgumentException(name);
                    }

                    bindings.put(name, deps.get(i));
                }

                try {
                    object = engine.eval(buildCall(call, names));
                } catch (ScriptException e) {
                    throw new RuntimeException(e);
                } finally {
                    names.forEach(bindings::remove);
                }

                return object;
            };
        }

        /**
         * Presents a JavaScript object as an instance of an interface.
         */
        private ObjectProvider proxyProvider(ObjectProvider jsObject, Class<?> proxyType) {
            return () -> {
                Object proxy = ((Invocable) engine).getInterface(jsObject.get(), proxyType);

                if (proxy == null) {
                    throw new IllegalArgumentException("cannot represent javascript object as "
                            + proxyType);
                }

                return proxy;
            };
        }

        private ObjectProvider jsObjectProvider(ObjectProvider jsObject) {
            return () -> new JsObject(engine, (ScriptObjectMirror) jsObject.get());
        }

        private List<Object> resolveDependencies(List<String> names) {
            List<Object> deps = names.stream()
                    .filter(nameToDep::containsKey)
                    .map(name -> nameToDep.get(name).getProvider().get())
                    .collect(Collectors.toList());

            if (deps.size() != names.size()) {
                Set<String> missing = new HashSet<>(names);
                missing.removeAll(nameToDep.keySet());

                throw new IllegalArgumentException("could not resolve dependencies " + missing);
            }

            return deps;
        }

        /**
         * Returns the list of dependency names specified by the {@code $inject}
         * property of the named function.
         *
         * @param function
         *         The name of the factory or constructor function.
         *
         * @return dependency names
         */
        private List<String> get$inject(String function) {
            ScriptObjectMirror $inject;

            try {
                $inject = (ScriptObjectMirror) engine.eval(function + ".$inject");
            } catch (ScriptException e) {
                throw new IllegalArgumentException("could not get " + function + ".$inject", e);
            }

            List<String> names = new ArrayList<>($inject.size());

            for (int i = 0, size = $inject.size(); i < size; i++) {
                names.add((String) $inject.get(Integer.toString(i)));
            }

            return names;
        }
    }

    private static String buildCall(String function, List<String> names) {
        StringBuilder sb = new StringBuilder(function).append("(");

        int startLength = sb.length();

        names.forEach(name -> {
            if (sb.length() > startLength) {
                sb.append(", ");
            }

            sb.append(name);
        });

        return sb.append(")").toString();
    }
}
