/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import java.lang.reflect.Field;
import java.util.Optional;

/**
 * Finds fields annotated {@link Dependency}.
 */
class DependencyMapper extends FieldMapper {
    @Override
    protected Optional<NamedProvider> accept(Object object, Field field) {
        Dependency dependency = field.getAnnotation(Dependency.class);

        if (dependency == null) {
            return Optional.empty();
        }

        String name;

        if (dependency.value().isEmpty()) {
            name = field.getName();
        } else {
            name = dependency.value();
        }

        return Optional.of(new NamedProvider(name, new DependencyField(object, field)));
    }
}
