/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import java.util.Objects;

import javax.script.Invocable;
import javax.script.ScriptEngine;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 * Wraps {@link ScriptObjectMirror} with some syntactic sugar.
 */
public class JsObject {

    private final ScriptEngine engine;

    private final ScriptObjectMirror jsObject;

    public JsObject(ScriptEngine engine, ScriptObjectMirror jsObject) {
        this.engine = Objects.requireNonNull(engine, "engine");
        this.jsObject = Objects.requireNonNull(jsObject, "js object");
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String propertyName) {
        return (T) jsObject.get(propertyName);
    }

    public boolean has(String propertyName) {
        return jsObject.containsKey(propertyName);
    }

    @SuppressWarnings("unchecked")
    public <T> T put(String propertyName, Object object) {
        return (T) jsObject.put(propertyName, object);
    }

    @SuppressWarnings("unchecked")
    public <T> T remove(String propertyName) {
        return (T) jsObject.remove(propertyName);
    }

    @SuppressWarnings("unchecked")
    public <T> T invokeMethod(String methodName, Object... args) {
        return (T) jsObject.callMember(methodName, args);
    }

    public <T> T as(Class<T> interfaceClass) {
        if (!interfaceClass.isInterface()) {
            throw new IllegalArgumentException(interfaceClass + " is not an interface");
        }

        T proxy = ((Invocable) engine).getInterface(jsObject, interfaceClass);

        if (proxy == null) {
            throw new IllegalArgumentException("incompatible interface " + interfaceClass.toString());
        }

        return proxy;
    }
}
