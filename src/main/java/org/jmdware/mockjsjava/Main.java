package org.jmdware.mockjsjava;

import java.io.InputStreamReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

public class Main {
    public static void main(String[] args) throws Exception {
        ScriptEngine nashorn = new ScriptEngineManager().getEngineByName("nashorn");

        SimpleBindings simpleBindings = new SimpleBindings();

        nashorn.eval(new InputStreamReader(Main.class.getResourceAsStream("Main.js")), simpleBindings);

        System.out.println(nashorn.eval("ctor", simpleBindings));

        nashorn.eval("new ctor(1, 2, { get: function() { return 'foo'; }})", simpleBindings);

        simpleBindings.put("xArg", 100);
        simpleBindings.put("yArg", 200);
        simpleBindings.put("svcArg", (Service) (() -> "got"));

        ScriptObjectMirror o = (ScriptObjectMirror) nashorn.eval("new ctor(xArg, yArg, svcArg)", simpleBindings);

        System.out.println(o.get("x"));
        System.out.println(o.get("y"));
        System.out.println(o.get("svc"));
        System.out.println(o.callMember("delegate"));
    }

    public interface Service {
        String get();
    }
}
