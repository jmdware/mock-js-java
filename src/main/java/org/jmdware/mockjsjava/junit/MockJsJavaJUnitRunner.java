/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava.junit;

import java.util.Arrays;
import java.util.Objects;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.jmdware.mockjsjava.InjectJsCtor;
import org.jmdware.mockjsjava.JsObjectMapper;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.mockito.MockitoAnnotations;

/**
 * <p>
 * Test runner that initializes {@link org.mockito.Mockito Mockito} and then
 * initializes the Nashorn JavaScript engine. Each {@link org.junit.Test @Test}
 * method has a separate instance {@link ScriptEngine} instance, which prevents
 * cross-test contamination.
 * </p>
 *
 * <p>
 * The test's member fields annotated with {@link org.jmdware.mockjsjava.Dependency Dependency}
 * are injected into {@link org.jmdware.mockjsjava.JsObject JsObject} fields
 * that have been annotated by {@link InjectJsCtor InjectJs}.
 * Fields that are interfaces may also be annotated with {@code InjectJs}.
 * </p>
 *
 * <pre>
 *
 * public class NumberManipulatorTest {
 *
 * }
 * </pre>
 */
public class MockJsJavaJUnitRunner extends BlockJUnit4ClassRunner {

    public MockJsJavaJUnitRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
    }

    @Override
    protected Statement withBefores(FrameworkMethod method, Object target, Statement statement) {
        return new MockJsJavaStatement(target, super.withBefores(method, target, statement));
    }

    public static void initialize(Object test) {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");

        loadIncludes(engine, test);
        injectJs(engine, test);
    }

    private static void loadIncludes(ScriptEngine engine, Object test) {
        MockJsJava mockJsJava = test.getClass().getAnnotation(MockJsJava.class);

        if (mockJsJava == null) {
            throw new IllegalArgumentException(test.getClass() + " is not annotated with "
                    + MockJsJava.class);
        }

        Arrays.asList(mockJsJava.value()).forEach(include -> {
            try {
                    /*
                     * Use load here so that ide's can honor breakpoints dropped
                     * in js files.
                     */
                engine.eval("load('" + include + "')");
            } catch (ScriptException e) {
                throw new IllegalArgumentException("could not load " + include, e);
            }
        });
    }

    private static void injectJs(ScriptEngine engine, Object test) {
        new JsObjectMapper(engine).map(test);
    }

    private static class MockJsJavaStatement extends Statement {

        private final Object test;

        private final Statement next;

        public MockJsJavaStatement(Object test, Statement next) {
            this.test = Objects.requireNonNull(test, "target");
            this.next = Objects.requireNonNull(next, "next");
        }

        @Override
        public void evaluate() throws Throwable {
            initializeMockito();
            initializeJavaScript();

            next.evaluate();
        }

        private void initializeMockito() {
            MockitoAnnotations.initMocks(test);
        }

        private void initializeJavaScript() {
            initialize(test);
        }
    }
}
