function ctor(x, y, svc) {
    this.x = x;
    this.y = y;
    this.svc = svc;
}

ctor.prototype = {
    x: 0,
    y: 0,
    delegate: function() {
        return this.svc.get();
    }
};
