/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a field as a JavaScript object created by a constructor function. May
 * only be applied to {@link JsObject} or a proxy interface.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface InjectJsCtor {
    /**
     * Returns the name of the JavaScript factory function that returns the
     * object to test.
     *
     * @return function name
     */
    String value();

    /**
     * Returns the names of the objects that will be passed as parameters to
     * {@link #value()}. Defaults to empty array.
     *
     * @return dependency names
     */
    String[] dependencies() default { };
}
