/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Iterates an object's fields and defers to the implementation of
 * {@link #accept(Object, Field)} to determine whether a field is a dependency
 * that can be used to build a JavaScript object.
 */
abstract class FieldMapper {
    final Map<String, ResolvableDependency> map(Object object) {
        Class<?> clazz = object.getClass();

        Set<String> names = new HashSet<>();

        Map<String, ResolvableDependency> nameToFactory = new HashMap<>();

        while (clazz != Object.class) {
            for (Field field : clazz.getDeclaredFields()) {
                if (!Modifier.isStatic(field.getModifiers())) {
                    if (!names.add(field.getName())) {
                        throw new IllegalArgumentException("duplicate field name from " + field);
                    }

                    field.setAccessible(true);

                    Optional<NamedProvider> provider = accept(object, field);

                    if (provider.isPresent()) {
                        String name = provider.get().name;

                        if (!(field.getName().equals(name))) {
                            if (!names.add(provider.get().name)) {
                                throw new IllegalArgumentException("duplicate dependency "
                                        + name + " from " + field);
                            }
                        }

                        ObjectProvider provider1 = provider.get().provider;

                        nameToFactory.put(name, new ResolvableDependency(field, provider1));
                    }
                }
            }

            clazz = clazz.getSuperclass();
        }

        return nameToFactory;
    }

    protected abstract Optional<NamedProvider> accept(Object object, Field field);

    protected final static class NamedProvider {

        private final String name;

        private final ObjectProvider provider;

        public NamedProvider(String name, ObjectProvider provider) {
            this.name = name;
            this.provider = provider;
        }
    }
}
