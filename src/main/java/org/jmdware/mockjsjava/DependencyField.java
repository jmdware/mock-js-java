/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import java.lang.reflect.Field;
import java.util.Objects;

class DependencyField implements ObjectProvider {

    private final Object object;

    private final Field field;

    DependencyField(Object object, Field field) {
        this.object = Objects.requireNonNull(object, "object");

        if (!field.getDeclaringClass().isAssignableFrom(object.getClass())) {
            throw new IllegalArgumentException(field + " cannot be applied to " + object);
        }

        this.field = Objects.requireNonNull(field, "field");
        this.field.setAccessible(true);
    }

    @Override
    public Object get() {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
