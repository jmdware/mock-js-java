/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * A dependency (usually a mock) and its field.
 */
class ResolvableDependency {

    private final Field field;

    private final ObjectProvider provider;

    public ResolvableDependency(Field field, ObjectProvider provider) {
        this.field = Objects.requireNonNull(field, "field");
        this.provider = Objects.requireNonNull(provider, "provider");
    }

    public Field getField() {
        return field;
    }

    public ObjectProvider getProvider() {
        return provider;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ResolvableDependency)) {
            return false;
        }

        ResolvableDependency that = (ResolvableDependency) o;

        return field.equals(that.field)
                && provider.equals(that.provider);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field, provider);
    }

    @Override
    public String toString() {
        return "ResolvableDependency{" +
                "field=" + field +
                ", provider=" + provider +
                '}';
    }
}
