mock-js-java provides junit support for testing JavaScript functions using the [nashorn](http://www.oracle.com/technetwork/articles/java/jf14-nashorn-2126515.html) javascript engine and [Mockito](http://mockito.org/) mock objects. A couple of highlights before digging into the details:

* Test factory functions and constructor functions.
* Inject Mockito objects into JavaScript functions.
* Use the provided junit runner or, instead, setup the JavaScript objects in an `@Before`.
* Define injections using `$inject` or in the test via annotation.
* Interact with the JavaScript object through the a proxy interface or through `JsObject`.

For example, say you have a JavaScript factory:

```
function NumberManipulator(num, adder, divider) {
    return {
        add: function(n) {
            return adder.add(num, n);
        },

        divide: function(n) {
            return divider.divide(num, n);
        }
    };
}
```

Using mock-js-java, you can test this factory using junit:

```
@RunWith(MockJsJavaJUnitRunner.class)
@MockJsJava("src/main/js/NumberManipulator.js")
public class NumberManipulatorTest {
    @Dependency
    private final int num  = 4;

    @Mock
    @Dependency
    private Adder adder;

    @Mock
    @Dependency
    private Divider divider;

    @InjectJsFactory("NumberManipulator", dependencies = { "num", "adder", "divider" })
    private NumberManipulator numberManipulator;

    @Test
    public void testAdd() {
        when(adder.add(4, 5)).thenReturn(20);

        assertThat(numberManipulator.add(5), equalTo(20));

        verify(adder).add(4, 4);
    }

    @Test
    public void testDivide() {
        when(divider.divide(4, 2)).thenReturn(2);

        assertThat(numberManipulator.divide(2), equalTo(2));

        verify(divider).divide(4, 2);
    }

    /**
     * Proxy interface used to expose a JavaScript object to Java.
     */
    public interface NumberManipulator {
        int add(int n);

        double divide(double n);
    }

    /**
     * Proxy interface used to mock a JavaScript object using Mockito. A mock
     * implementation is injected into the JavaScript object-under-test.
     */
    public interface Adder {
        int add(int n1, int n2);
    }

    /**
     * Proxy interface used to mock a JavaScript object using Mockito. A mock
     * implementation is injected into the JavaScript object-under-test.
     */
    public interface Divider {
        double divide(double n1, double n2);
    }
}
```
See the [factory](src/test/java/org/jmdware/mockjsjava/example/factory) directory and [constructor](src/test/java/org/jmdware/mockjsjava/example/constructor) examples.

mock-js-java uses annotations to bridge the Java and JavaScript worlds. Annotate a field with `@InjectJsFactory` or `@InjectJsCtor` to tell mock-js-java to capture the result of a factory or constructor function, respectively, in that field.

```
@InjectJsFactory("NumberManipulator", dependencies = { "num", "adder", "divider" })
private NumberManipulator numberManipulator;

```

In this example, we are capturing the value returned by the factory function `NumberManipulator` as an instance of a java interface, with the same name in this example. The dependencies are explicitly defined on the annotation, but we can remove the `dependencies` property if we express this same information in JavaScript using `$inject`:

```
function NumberManipulator(num, adder, divider) {
    return {
        add: function(n) {
            return adder.add(num, n);
        },

        divide: function(n) {
            return divider.divide(num, n);
        }
    };
}

// Remember to setup your JavaScript minifier to preserve $inject.
NumberManipulator.$inject = [ 'num', 'adder', 'divider' ];
```

With this JavaScript, the field in the test can be annotated simply:

```
@InjectJsFactory("NumberManipulator")
private NumberManipulator numberManipulator;

```

JavaScript constructor support is enabled using the `@InjectJsCtor` annotation:

```
@InjectJsCtor("NumberManipulator", dependencies = { "num", "adder", "divider" })
private NumberManipulator numberManipulator;
```

The `dependencies` property defines an ordered array of dependency names. The actual dependencies are defined using the `@Dependency` annotation.

```
@Dependency
private final int num = 4;

@Mock
@Dependency("adder")
private Adder adderMock;

@Mock
@Dependency
private Divider dividerMock;

```

Here, we have three dependencies. By default, the name of a dependency is the name of the field to which the annotation is attached. To support situations where the Java field name differs from the JavaScript name, `@Depenency` accepts an explicit name. In this example, the JavaScript name of the dependency is `adder` while the Java field is named `adderMock`.
